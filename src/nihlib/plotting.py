#! /bin/env python3
# coding: utf-8

# nihlib
#
# Author(s):
# * Ale Abdo <abdo@member.fsf.org>
#
# License:
# [GNU-GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)
#
# Contributions are welcome, get in touch with the author(s).


from matplotlib import pyplot as plt

plt.rcParams["figure.figsize"] = [16, 9]
plt.rcParams["xtick.labelsize"] = "large"


# TODO: move generic plot code from nih_overview.ipynb
