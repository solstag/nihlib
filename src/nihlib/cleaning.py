#! /bin/env python3
# coding: utf-8

# nihlib
#
# Author(s):
# * Ale Abdo <abdo@member.fsf.org>
#
# License:
# [GNU-GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)
#
# Contributions are welcome, get in touch with the author(s).


from . import regex
import pandas as pd

# Play well with notebooks
from IPython.display import Markdown, display


##################################
# Data cleaning and manipulation #
##################################


def get_abstracts_indexed_like_prj_on_selection(prjsel, prjabs):
    """
    Match projects in prj, usually on a selection on it, to their abstracts in prjabs.

    For that, 'APPLICATION_ID's must be unique on prj and prjabs. For this reason,
    this function is rarely called on prj directly, but rather on a selection with
    unique 'APPLICATION_ID's.

    Ex.:
    ```
    get_abstracts_indexed_like_prj_on_selection(
        prj[useful_selections['b_nci_r01_new_le2018']], prjabs)`
    ```

    Parameters
    ----------
    prjsel: pandas.DataFrame
        Output of `exporter_load_prj` or a selection on it.
    prjabs: pandas.DataFrame
        Output of `exporter_load_prjabs`.

    Returns
    -------
    pandas.DataFrame:
        abstracts from `prjabs` indexed like `prj` on selection `psel`
    """
    # Get prjsel APPLICATION_ID's to link with prjabs
    assert prjsel["APPLICATION_ID"].is_unique
    prjsel_aids = prjsel["APPLICATION_ID"].values
    asel = prjabs["APPLICATION_ID"].isin(prjsel_aids)
    # linking requires uniqueness on both sides
    assert prjabs.loc[asel, "APPLICATION_ID"].is_unique
    abs_idx_prjsel = (
        prjabs[asel]  # select only abstracts for prjsel
        .set_index("APPLICATION_ID")  # make APPLICATION_ID the index
        .reindex(prjsel_aids)  # order like prjsel
        .set_index(prjsel.index)  # replace by prjsel's index
    )
    return abs_idx_prjsel


def replace_empty_abstracts(abstracts, prj, prjabs, siblings):
    """
    Sets empty abstracts to `None`, then try replacing each by a later abstract
    from the same project, the earliest that is not empty.

    Parameters
    ----------
    abstracts: pandas.DataFrame
        Abstracts for a selection on `prj`, indexed like it.
    prj: pandas.DataFrame
        Output of `exporter_load_prj`.
    siblings: a selector for `prj`
        The applications to be considered for abstract replacement, usually the
        same type of project. Ex. `useful_selections['b_nci_r01']`.

    Returns
    -------
    list:
        Index of `prj` items whose abstracts got replaced.
    """
    # Set empty and easy to spot false abstracts to None
    b_empty = abstracts["ABSTRACT_TEXT"].str.contains(regex.RE_NONABS, na=True)
    abstracts.loc[b_empty, "ABSTRACT_TEXT"] = None
    display("Empty abstracts set to `None`: {}".format(b_empty.sum()))

    interpolated = []
    for i in abstracts[abstracts["ABSTRACT_TEXT"].isna()].index:
        p_cpn = prj.loc[i, "CORE_PROJECT_NUM"]
        p_sel = siblings & prj["CORE_PROJECT_NUM"].eq(p_cpn)
        p_aids = prj.loc[p_sel, "APPLICATION_ID"]
        a_sel = prjabs["APPLICATION_ID"].isin(p_aids)
        candidates = prjabs.loc[a_sel]
        c_sel = ~candidates["ABSTRACT_TEXT"].str.contains(regex.RE_NONABS, na=True)
        candidates = candidates.loc[c_sel]
        if not candidates.empty:
            source = candidates.sort_values("APPLICATION_ID").iloc[0]
            abstracts.loc[i, "ABSTRACT_TEXT"] = source.loc["ABSTRACT_TEXT"]
            interpolated.append(i)
    display("`None` abstracts replaced: {}".format(len(interpolated)))
    return interpolated


def get_clean_abstracts(abstracts, extract=False, verbose=False):
    """
    Extracts from `abstracts` the different parts of an abstract.

    Parameters
    ----------
    abstracts: pandas.DataFrame
        Abstracts for `prj` indexed like it.
    """
    # Prepare abstracts to extract sections
    abs_pre = (
        abstracts["ABSTRACT_TEXT"]
        .str.replace(regex.RE_NONTEXT, "")
        .str.strip()
        .str.replace(r"\s+", " ")  # trim and remove multiple whitespace
    )
    if verbose:
        display("Most repeated abstract heads")
        display(
            abs_pre.str.slice(0, 200)
            .value_counts()
            .sort_values()
            # .value_counts() # uncomment to show how many repetitions have given frequencies
            .tail(10)
        )
        display("Short abstracts")
        display(abs_pre[abs_pre.str.len() < 300])

    if not extract:
        return abs_pre
    return abs_pre.str.extract(regex.RE_SECTIONS).apply(lambda x: x.str.strip())


def get_prj_narratives(prj, abstract_sections, onlyna=False):
    """
    Get the narratives from `prj` for the abstracts in abstract_sections.

    If `onlyna` is passed, restrict to abstracts with no narrative section.

    Parameters
    ----------
    prj: pandas.DataFrame
        Output of `exporter_load_prj`.
    abstract_sections: pandas.DataFrame
        Abstracts with sections extracted, output of `get_clean_abstracts`.
    onlyna: bool
        Restrict to abstracts with no narrative section.

    Returns
    -------
    narratives: DataFrame
    conflicts: selector for narratives, returned if onlyna==False.
    """
    sel = abstract_sections.index[
        abstract_sections[3].isna() if onlyna else slice(None)
    ]
    narratives = (
        prj.loc[sel, "PHR"]
        .str.replace(regex.RE_NONTEXT, "")
        .str.strip()
        .str.replace(r"\s+", " ")  # trim and remove multiple whitespace
        .str.extract(regex.RE_SECTIONS_PHR)
        .apply(lambda x: x.str.strip())
    )
    if onlyna:
        conflicts = abstract_sections[3].notna() & narratives[2].notna()
        display("Conflicts: {}".format(conflicts.sum()))
        return narratives, conflicts
    return narratives
