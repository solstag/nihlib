# nihnih
A python library to work with NIH data.

Download, consolidate and clean datasets from NIH's Exporter.

## Mode d'emploi
- If you create or edit `.py` files, please [black](https://black.readthedocs.io/) them before committing.

## License
Contents of this repository are released under the GNU GPL version 3 or later.
See the file [COPYING](./COPYING) for the terms of the license.

