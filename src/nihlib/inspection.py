#! /bin/env python3
# coding: utf-8

# nihlib
#
# Author(s):
# * Ale Abdo <abdo@member.fsf.org>
#
# License:
# [GNU-GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)
#
# Contributions are welcome, get in touch with the author(s).


import os
from os import path
from datetime import datetime

from . import OUT_DIR


#################################
# Data selection and inspection #
#################################


def getfun(x):
    """
    Process a "FUNDING_ICS" column's cell, returning a tuple with the NIH code
    for each funder listed.
    """
    fund = []
    if isinstance(x, str):
        for y in x.strip("\\").split("\\"):
            fund.append(y.split(":")[0])
    return tuple(fund)


def get_useful_selections(prj, r01e=True):
    """
    Returns a dictionary with selectors for intersting categories and
    combinations.

    Parameters
    ----------
    prj: dataframe of project applications (dataset PRJ)
    r01e: whether to search for "r01 equivalent" applications (see `is_r01equivalent`)

    Returns
    -------
    `dict` containing the following categories and a few combinations:

    cor_uni: keep only first appearence of core project number
    app_uni: keep only first appearance of application id
    r01: R01 applications
    new: new applications
    nci_adm: NCI is administering
    nci_fun: NCI is a funder
    le2018: fiscal year from 2018 backwards
    le2018: fiscal year from 2019 backwards
    r01e: R01 equivalent applications (if r01e=True)
    """
    b_cor_uni = ~prj["CORE_PROJECT_NUM"].duplicated()
    b_app_uni = ~prj["APPLICATION_ID"].duplicated()
    b_new = prj["APPLICATION_TYPE"].eq(1)
    b_nci_adm = prj["ADMINISTERING_IC"].eq("CA")
    b_nci_fun = prj["FUNDING_ICS"].transform(lambda x: "NCI" in getfun(x))
    b_le2018 = prj["FY"].le(2018)
    b_le2019 = prj["FY"].le(2019)
    b_r01 = prj["ACTIVITY"].eq("R01")
    # combinations
    b_nci = b_nci_fun | b_nci_adm
    b_nci_new = b_nci & b_new
    b_nci_r01 = b_nci & b_r01
    b_nci_r01_new = b_nci_r01 & b_new
    b_nci_r01_new_le2018 = b_nci_r01_new & b_le2018
    if r01e:
        b_r01e = prj.agg(is_r01equivalent, axis=1)
        b_nci_r01e = b_nci & b_r01e
        b_nci_r01e_new = b_nci_r01e & b_new
        b_nci_r01e_new_le2019 = b_nci_r01e_new & b_le2019
    # build dict and return
    usel = locals()
    for x in ["prj", "r01e"]:
        usel.pop(x)
    return usel


def is_r01equivalent(prj_row):
    """
    Whether an application (a row in the PRJ dataframe) is R01 equivalent.

    Here we use the definition provided by the NIH by e-mail, detailing the definition found on their website.

    Example
    -----
    `b_r01e = prj.agg(is_r01equivalent, axis=1)` to get a selector.
    """
    r01e = {"R01", "R23", "R29", "R37"}
    r01e_r35foa = {"RFA-GM-16-003", "RFA-GM-17-004", "PAR-17-094", "PAR-17-190"}
    year = prj_row["FY"]
    if year >= 2016:
        r01e.update(["RF1"])
        if year >= 2017:
            r01e.update(["DP2"])
        if year >= 2018:
            r01e.update(["DP1", "DP5", "R56", "RL1", "U01"])
            if (prj_row["ACTIVITY"] == "R35") and (
                prj_row["FOA_NUMBER"] in r01e_r35foa
            ):
                return True
    return prj_row["ACTIVITY"] in r01e


def check_prj_prjabs(sel, prj, prjabs):
    """
    Check whether for a given selection:
    - `prj` restricted to the selection has unique APPLICATION_IDs
    - `prjabs`, restricted to those APPLICATION_IDs, has unique APPLICATION_IDs

    Parameters
    ----------
    sel: a selector
    prj: dataframe of project applications (dataset PRJ)
    prjabs: dataframe of abstracts (dataset PRJABS)

    Returns
    -------
    aids:
        The APPLICATION_IDs of the selection on `prj`.
    abssel:
        A selector for `prjabs`, selecting abstracts with APPLICATION_ID corresponding
        to a selected application in `prj`.
    """
    assert prj.loc[sel, "APPLICATION_ID"].is_unique
    aids = prj[sel]["APPLICATION_ID"].values
    abssel = prjabs["APPLICATION_ID"].isin(aids)
    assert prjabs[abssel]["APPLICATION_ID"].is_unique
    return aids, abssel


def compare_selectors(x, y):
    """
    Given two selections, get their sizes, and those of their subtractions,
    union and intersection, as an `OrderedDict`.

    Examples
    --------
    compare_selectors( (b_nci_fun | b_nci_adm), b_new)
    """
    from collections import OrderedDict

    res = OrderedDict(
        [
            ("A", x.sum()),
            ("B", y.sum()),
            ("AmB", (x & ~y).sum()),
            ("BmA", (y & ~x).sum()),
            ("AuB", (x | y).sum()),
            ("AiB", (x & y).sum()),
        ]
    )
    return res


def get_sample(df, sel, num, name="sample"):
    """
    Sample `num` applications from a projects dataframe's selection, ensuring
    that the dataframe's "APPLICATION_ID" column is unique. Stores the sample
    into a file of the form "sample-2019-10-16T01:10:23.870.csv".

    TODO: check if we can locate the abstract for a project otherwise resample.
    (MUST resample from the same fiscal year!)
    """
    assert df.loc[sel, "APPLICATION_ID"].is_unique
    now = datetime.now().isoformat(timespec="milliseconds")
    fname = path.extsep.join(["_".join([name, num, now]), ".csv"])
    fpath = path.join(OUT_DIR, fname)
    os.makedirs(OUT_DIR, exist_ok=True)
    prj[sel].sample(num).to_csv(fpath, index=False)
