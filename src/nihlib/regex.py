#! /bin/env python3
# coding: utf-8

# nihlib
#
# Author(s):
# * Ale Abdo <abdo@member.fsf.org>
#
# License:
# [GNU-GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)
#
# Contributions are welcome, get in touch with the author(s).

"""
Regular expressions used in `nihlib`.

Kept apart from the code so we don't accidentally modify them.
"""

#######################
# Acquisition related #
#######################


def get_wget_accept_regex(years):
    """
    Parameters
    ----------
    years: list of years
        Which years to download from the source, if applicable.
        The regex is built in such a way that if a source is not split into
        years then the unsplit file will be downloaded.

    Returns
    -------
    A string appropriate for `wget --accept-regex`.
    """
    if years is None:
        return r"_C_.*\.zip"
    else:
        re_years = "|".join(filter(None, map(str, years)))
        return r"_C_ ( (FY)?({}) | ALL ) .* \.zip".format(re_years).replace(" ", "")


def get_exporter_file_regex(dataset, years):
    if years is None:
        return "{}_C_.*\.zip".format(dataset)
    else:
        re_years = "|".join(filter(None, map(str, years)))
        return r"(?x) {}_C_ ( (FY)?({}) | ALL ) .* \.zip".format(dataset, re_years)


#################################
# Abstract cleaning and parsing #
#################################

# Absent abstracts
RE_NONABS = r"(?i)(?:^\s*$|No\s*Abstract\s*Provided|ABSTRACT\s*NOT\s*PROVIDED)"

# Unreadable parts inside valid abstracts
RE_NONTEXT = r"""(?xi)
    \[ \s* unreadable \s* \]
    |
    Page \s* Continuation \s* Format \s* Page
    |
    \( \s* See \s* inst [A-z]+ ctions \s* \)
"""

# References to author in section introductions
RE_AUTHOR = r"(?: applicant|application|author|investigator )"

RE_ABSTRACT = r"(?: abstract|application|description )"

RE_PROVIDED = "(?: PROVIDED \s* BY (?: \s* THE)? \s* {} )".format(RE_AUTHOR)

RE_PARENTHESIS = r"(?: \( [\s\w']* \) )"

# Abstract section introduction
RE_INTRO = (
    r"(?: "
    + r"|".join(
        [
            r"ABSTRACT",
            r"TITLE",
            r"PROJECT",
            r"SUMMARY",
            r"SPACE \s* PROVIDED",
            RE_PROVIDED,
            r"RESEARCH \s* :",
            r"D\ ?ES?CRIPTION",
            r"""
            (?: AS \s* )?
            (?:VERBATIM \s* | AD(?:AP|PA)TED \s* | ADAPT \s* | TAKEN \s*)+
            FROM (?: \s* THE )? (?: \s* principal )? \s*
            (?: {RE_AUTHOR} ['s\s]* )?
            {RE_ABSTRACT} ? (?: \s* AND SPECIFIC AIMS )?
        """.format(
                RE_AUTHOR=RE_AUTHOR, RE_ABSTRACT=RE_ABSTRACT
            ),
            RE_AUTHOR + r"['s\s]* " + RE_ABSTRACT,
            r"(?: AS \s* )? PER \s* " + RE_ABSTRACT,
            RE_PARENTHESIS,
        ]
    )
    + r" )"
)

# Narrative (Public Health Relevance) section introduction
RE_NARRATIVE_S = (
    r"(?: "
    + r"|".join(
        map(
            lambda x: r"(?:THE \s*)? (?:PROJECT['S]* \s*)? " + x,
            [
                r"STATEMENT",
                r"NARRATIVE (?:\W*SUMMARY)?",
                r"INTEREST",
                r"SIGNIFICANCE",
                r"IMPACT",
                r"(?:TO\s*|TO\s*THE\s*)? PUBLIC \s* (?:HE[A-Z]+TH)?",
                r"(?:TO\s*)? HUMAN \s* (?:HE[A-Z]+TH)",
                r"(?<!P)RE[LV]\w[^TS][A-Z]+", # RELEVANCE minus similar words
            ],
        )
    )
    + r" )"
)

RE_NARRATIVE_I = r"""(?:
    (?: {RE_NARRATIVE_S} \W* ){{2,}}
    (?: including \s* health-related \s* significance \s* )? \W*
    |
    (?: (?<=[^\s\w]) | ^ ) \s* {RE_NARRATIVE_S} \s* (?:SUMMARY)? \s* [:;]
    |
    ^ (?:
        \s* (?:THE \s*)? (?:PROJECT['S]* \s*)? NARRATIVE \s* (?:SUMMARY)? \s*
        |
        \W* {RE_PROVIDED} \W*
    )
)""".format(
    RE_NARRATIVE_S=RE_NARRATIVE_S, RE_PROVIDED=RE_PROVIDED
)

RE_NARRATIVE = r"""
    (?:
        (?: (?<=[^\s\w]) | ^ )
        [A-Z\s]*
    )?
    (?: {RE_NARRATIVE_S} \W* )+
    (?i: {RE_NARRATIVE_I} )? \W*
    (?i: {RE_PROVIDED} )? \W*
    |
    (?:
        (?:
            (?: (?<=[^\s\w]) \s* | ^ [^A-z]* )

        )
        |
        (?=[A-Z])
    )
    (?i: {RE_NARRATIVE_I} ) \W*
    (?i: {RE_NARRATIVE_I} )? \W*
    (?i: {RE_PROVIDED} )? \W*
""".format(
    RE_NARRATIVE_I=RE_NARRATIVE_I, RE_PROVIDED=RE_PROVIDED, RE_NARRATIVE_S=RE_NARRATIVE_S
)

# Section introduction/separators
RE_SECTIONS = r"""(?x)^
    (
        [^A-z]*
        (?:
            (?i: {RE_INTRO} )
            \W*
        )+
    )?
    (.*?)
    (?:
        ( {RE_NARRATIVE} )
        (.*)
    )? $""".format(
    RE_INTRO=RE_INTRO, RE_NARRATIVE=RE_NARRATIVE
)

RE_SECTIONS_PHR = r"""(?x)^
    (.*?)
    (?:
        ( {RE_NARRATIVE} )
        (.*)
    )? $""".format(
    RE_NARRATIVE=RE_NARRATIVE
)

# Useful to test these regexes:
# query = r"(?x)(.{,40})(" + nihlib.regex.RE_NARRATIVE + r")(.{,40})" ;
# df.str.extract( query )
# df[ df.str.contains(query) ].str.extract(query)

