#! /bin/env python3
# coding: utf-8

# nihlib
#
# Author(s):
# * Ale Abdo <abdo@member.fsf.org>
#
# License:
# [GNU-GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)
#
# Contributions are welcome, get in touch with the author(s).


import os, re, subprocess
import pandas as pd
from os import path
from datetime import datetime
from zipfile import BadZipFile

from . import DATA_DIR, DATA_TYPES, PARSE_DATES, SOURCE_INDEX
from .regex import get_wget_accept_regex, get_exporter_file_regex

####################
# Data acquisition #
####################


def exporter_download(sources=SOURCE_INDEX.keys(), years=None):
    """
    Downloads data from all or specified sources.

    Available sources are:
        "projects", "abstracts", "publications", "patents",
        "clinical studies", "link tables".
    Subsequent runs will only download new files.

    Parameters
    ----------
    sources: list of strings (default: all sources)
        List of sources to download.
    years: list of ints (default: all years)
        List of years to download data for. We currently download based on the
        year in the file name, which in the presence of monthly updates may
        overflow the fiscal year (october to september).
    """
    os.makedirs(DATA_DIR, exist_ok=True)
    wget_accept_regex = get_wget_accept_regex(years)
    for source in sources:
        index = SOURCE_INDEX[source]
        target = "exporter.nih.gov/ExPORTER_Catalog.aspx?index={}".format(index)
        wget_log = "wget_{}_{}.log".format(
            datetime.now().isoformat().replace(":", "."), source  # W$ can't ":"
        )
        command = [
            "wget",
            path.join("https://", target),
            "-a",
            wget_log,
            "-nc",
            "-r",
            "--accept-regex",
            wget_accept_regex,
        ]
        status = "unknown"
        print("COMMAND:", " ".join(command))
        try:
            cproc = subprocess.run(command, cwd=DATA_DIR)
            if cproc.returncode == 0:
                status = "complete"
        except KeyboardInterrupt:
            status = "interrupted"
            print("\n\nManually interrupted!")
            break
        finally:
            try:
                os.rename(
                    path.join(DATA_DIR, target),
                    path.join(DATA_DIR, path.extsep.join([target, status])),
                )
            except FileNotFoundError:
                status = "nodownload"
                print(
                    "\nData not downloaded/updated, please check your"
                    " connection to the Internet."
                )
            if status not in ("complete", "nodownload"):
                print(
                    "\nWarning: Downloading was incomplete, you must check",
                    path.join(DATA_DIR, wget_log),
                    "and manually remove truncated files before retrying.",
                )


def exporter_load(dataset, years=None):
    """
    Loads one of the downloaded datasets as a dataframe, optionally restricting to files
    labeled with a given list of years.

    Parameters
    ----------
    dataset: string
        The choice of dataset to load.

    Returns
    -------
    A tuple containing the dataframe plus a series indicating the source file
    for each row.

    Datasets
    -------
    For source "projects" (fiscal year):
        PRJ: general project data
        PRJFUNDING: updates and corrections to funding and DUNS records in PRJ
        DUNS: corrections to DUNS records in PRJ
    For source "abstracts" (fiscal year):
        PRJABS: text of project abstracts
    For source "publications" (calendar year):
        PUB: publications associated with projects
        AFFLNK: updates with author affiliations
    For source "patents":
        PATENTS: data on project patents
    For source "clinical studies":
        CLINICAL_STUDIES: data on clinical studies
    For source "link tables":
        PUBLNK: link tables for publication data

    To understand these datasets: <https://exporter.nih.gov/faq.aspx>
    """
    print("Loading {}:".format(dataset), end=" ")
    dfs = []
    sources = []
    fdir = path.join(DATA_DIR, "exporter.nih.gov/CSVs/final")
    rex_file = re.compile(get_exporter_file_regex(dataset, years))
    for fname in filter(rex_file.search, sorted(os.listdir(fdir))):
        print(fname, end="")
        try:
            df = pd.read_csv(
                path.join(fdir, fname),
                low_memory=False,
                encoding="latin_1",
                dtype=DATA_TYPES,
                parse_dates=PARSE_DATES[dataset],
                infer_datetime_format=True,
            )
        except BadZipFile:
            print(" (**Corrupted**) (Skipping)", end=", ")
            continue
        # FUNDING_ICS are inconsistently named among datasets
        df.rename(
            {"FUNDING_Ics": "FUNDING_ICS", "FUNDING_ICs": "FUNDING_ICS"},
            axis=1,
            inplace=True,
        )
        source = pd.Series(fname, index=df.index)
        dfs.append(df)
        sources.append(source)
        if df.duplicated().sum():
            print(" (Has duplicates!)")
        print(", ", end="", flush=True)
    print("\b\b.")
    if dfs:
        df = pd.concat(dfs, ignore_index=True, sort=True)
        source = pd.concat(sources, ignore_index=True, sort=True)
        dfd = df.duplicated()
        df, source = df[~dfd], source[~dfd]
        print("Duplicates: {}".format(dfd.sum()))
    else:
        df = pd.DataFrame(columns=["APPLICATION_ID"])
        source = pd.Series()
        print("Nothing to load for {}.".format(dataset))
    return df, source


def exporter_load_prj(years=None):
    """
    Load all datasets from source "projects" and apply corrections and
    updates, returning the consolidated dataframe.
    """
    prj, prjsrc = exporter_load("PRJ", years)
    prjfun, _ = exporter_load("PRJFUNDING", years)
    prjdun, _ = exporter_load("DUNS", years)

    # update `prj` with info with `prjfun` and `prjdun`
    prjfun = prjfun.set_index("APPLICATION_ID", verify_integrity=True)
    prjdun = prjdun.set_index("APPLICATION_ID", verify_integrity=True)
    prj = prj.set_index(["APPLICATION_ID"])
    prj.loc[prj.index.intersection(prjfun.index), prjfun.columns] = prjfun
    prj.loc[prj.index.intersection(prjdun.index), prjdun.columns] = prjdun
    prj = prj.reset_index()

    return prj, prjsrc


def exporter_load_prjabs(years=None):
    prjabs, prjabssrc = exporter_load("PRJABS", years)
    return prjabs, prjabssrc
