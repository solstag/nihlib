#! /bin/env python3
# coding: utf-8

# nihlib
#
# Author(s):
# * Ale Abdo <abdo@member.fsf.org>
#
# License:
# [GNU-GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)
#
# Contributions are welcome, get in touch with the author(s).


"""
nihlib
======

Tools to work with NIH data from <https://exporter.nih.gov/>.

Submodules
----------

- acquisition
- cleaning (has bugs)
- inspection
- plotting (not yet)
- regex


Usage
-----

```
import nihlib

nihlib.acquisition.exporter_download(['projects'])

prj, prjsrc = nihlib.acquisition.exporter_load_prj()

sels = nihlib.inspection.get_useful_selections(prj)

nihlib.inspection.compare_selectors(
    sels['b_r01'],
    sels['b_new'],
)

nihlib.inspection.get_sample(
    prj,
    sels['b_nci'],
    10,
)

```

"""


__author__ = "Alexandre Hannud Abdo <abdo@member.fsf.org>"
__copyright__ = "Copyright 2020 Alexandre Hannud Abdo"
__license__ = "GNU GPL version 3 or above"
__URL__ = "https://gitlab.com/solstag/nihnih/"


__all__ = [
    "acquisition",
    "cleaning",
    "inspection",
    # 'plotting',
    "regex",
]

# require Python3.6 as regular expressions used for cleaning employ local flags
import sys as _sys

assert _sys.version_info >= (3, 6), "nihlib requires Python 3.6 or later"


from os import path as _path

# Play well with notebooks
from IPython.display import display as _display


#################
# Configuration #
#################


SOURCE_INDEX = {
    "projects": 0,
    "abstracts": 1,
    "publications": 2,
    "patents": 3,
    "clinical studies": 4,
    "link tables": 5,
}

DATA_TYPES = {"CFDA_CODE": str}

PARSE_DATES = {
    "PRJ": [
        "AWARD_NOTICE_DATE",
        "BUDGET_END",
        "BUDGET_START",
        "PROJECT_START",
        "PROJECT_END",
    ],
    "PRJFUNDING": [],
    "DUNS": [],
    "PRJABS": [],
}

DATA_DIR = _path.normpath("data")

OUT_DIR = _path.normpath("products")

_display("Data will be saved to: {}".format(DATA_DIR))
_display("Products will be saved to: {}".format(OUT_DIR))


#####################
# Expose submodules #
#####################


from . import acquisition
from . import cleaning
from . import inspection

# from .plotting import * # still on the move
from . import regex
